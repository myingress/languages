# What is a .PO file?
A .PO file is a portable object file, which is text-based. These types of files are used in commonly in software development. The .PO file may be referenced by Java programs, GNU gettext, or other software programs as a properties file. These files are saved in a human-readable format so that they can be viewed in a text editor by engineers and translators.

“GetText Portable Object (PO) files are the industry standard for multilingual websites in PHP” - icanlocalize 

# What is a .POT file?
A .POT is a Portable Object Template, which is also text-based.

# What is the difference?
We are often asked: what is the difference between a .PO file and a .POT file?

Well, they are really the same file. The only difference being their final intended use. 

# Benefits
1. File Output in the Correct Format
One of the great benefits when working with .PO files is the fact that you have an immediate file output and in the right format to send for localization. And once the translations are completed, there is no manual back conversion required. This means savings in time and money which should be reflected in the quote you get for the project.

If your company uses WordPress, it might interest you to know that “WPML (The WordPress Multilingual Plugin) includes a robust PHP scanner and can produce these .POT files for you.

You can get the WPML Plugin here. 

# A bit about PHP
PHP is a Hypertext Preprocessor (although originally the acronym stood for Personal Home Page).
PHP is a widely-used open source general-purpose scripting language. This language is especially suited for web development.
PHP can be embedded into HTML, or it can be used in combination with web content management systems.
GNU gettext really helps internationalize PHP projects managed as part of a content-management framework such as (Drupal) or WordPress CMS for example.
PHP can be used to extract strings from many other programming languages such as C, C++, PO, Python, Java, JavaProperties, etc. etc.
2. Human Readable and Editable
Another benefit of the .PO file is that is it compact, human-readable and editable without requiring any special-purpose tool.

Although translators will often prefer to work on .PO files using dedicated .PO editors, such as Poedit, they should be able to comprehend the PO format. This is because the format is more than a container of the text to be translated, instead it reflects important concepts in the translation workflow.

“To put it more concretely, the translator should determine in advance how a given dedicated PO editor exposes the bits of information from the PO file in its interface, and whether it truly exposes all of them.” - http://pology.nedohodnik.net

The result is a .PO file with the same format as a .POT, but with translations in place and with language specific headers.